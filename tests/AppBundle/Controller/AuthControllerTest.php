<?php

namespace AppBundle\Tests\Controller;

use Lakion\ApiTestCase\JsonApiTestCase;
use GuzzleHttp\Client;

class AuthControllerTest extends JsonApiTestCase
{

    public function testGetToken()
    {
        $this->client->request('POST', 'http://stg.football.com/auth/get-token',["username" => "football"]);

        $response = $this->client->getResponse();
        $responseArr = json_decode($response->getContent(), true);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame("1", $responseArr['status']);

        if(200 == $response->getStatusCode() && "1" == $responseArr['status'])
        {
        	return $responseArr['token'];
        }

        return false;
    }
}