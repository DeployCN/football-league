<?php

namespace AppBundle\Tests\Controller;

use Lakion\ApiTestCase\JsonApiTestCase;
use GuzzleHttp\Client;
use AppBundle\Tests\Controller\AuthControllerTest;
use GuzzleHttp\RequestOptions;

class LeagueControllerTest extends AuthControllerTest
{

    public function testListTeamsAction()
    {

        $token = $this->testGetToken();
        $this->client->request(
            'GET', 
            'http://dev95.developer24x7.com/football-league/web/api/league/2',  
            [],
            [],
            array('HTTP_AUTHORIZATION' => $token)
        );

        $response = $this->client->getResponse();

        $json = $response->getContent(true);
        $responseArr = json_decode($json, true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame("1", $responseArr['status']);
        $this->assertResponse($response, 'league_teams');

    }

    public function testDeleteLeagueAction()
    {
        $client = new Client();
        $token = $this->testGetToken();

        $response = $client->delete('http://dev95.developer24x7.com/football-league/web/api/league/1',[
                    'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $token
                    
                ],
                'form_params' => [
                    'id' => '31'
                ]
        ]);

        $json = $response->getBody(true);
        $responseArr = json_decode($json, true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame("1", $responseArr['status']);

        
    }
}