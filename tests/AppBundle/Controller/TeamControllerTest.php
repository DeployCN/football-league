<?php

namespace AppBundle\Tests\Controller;

use Lakion\ApiTestCase\JsonApiTestCase;
use GuzzleHttp\Client;
use AppBundle\Tests\Controller\AuthControllerTest;
use GuzzleHttp\RequestOptions;
class TeamControllerTest extends AuthControllerTest
{

    public function testAddTeamAction()
    {
        $client = new Client();
        $token = $this->testGetToken();

        $response = $client->post('http://dev95.developer24x7.com/football-league/web/api/league/team',[
                    'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $token
                    
                ],
                'form_params' => [
                    'name' => 'Test Team'
                ]
        ]);

        $json = $response->getBody(true);
        $responseArr = json_decode($json, true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame("1", $responseArr['status']);

        
    }


    public function testUpdateTeamAction()
    {
    	$client = new Client();
        $token = $this->testGetToken();

        $response = $client->put('http://dev95.developer24x7.com/football-league/web/api/league/team',[
                    'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $token
                    
                ],
                'json' => [
			        'id' => "2",
			        'name' => "Santuuuuuuuuu"
			    ]
        ]);

        $json = $response->getBody(true);
        $responseArr = json_decode($json, true);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame("1", $responseArr['status']);
    }

    
}