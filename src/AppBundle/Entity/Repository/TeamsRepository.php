<?php


namespace AppBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use AppBundle\Entity\Teams;


class TeamsRepository extends EntityRepository
{

    public function addTeam($data)
    {
        $team = new Teams();
        $team->setName($data['name'])
            ->setStrip($data['strip']);
        $this->_em->persist($team);
        $this->_em->flush();
        $this->_em->clear();
        return $team->getId();

    }

    public function updateTeam($data)
    {
        //$em = $this->getEntityManager();
        $team = $this->find($data['id']);

        $team->setName($data['name'])->setStrip($data['strip']);

        $this->_em->persist($team);
        $this->_em->flush();
        $this->_em->clear();

        return $team->getId();
    }
}
