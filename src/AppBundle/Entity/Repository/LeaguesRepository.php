<?php


namespace AppBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Common\Collections\ArrayCollection;


class LeaguesRepository extends EntityRepository
{
    
    public function findTeamsByLeagueId($id)
    {
        
        $league = $this->createQueryBuilder('l')
            ->select('partial l.{id,name}, partial t.{id, name}')
            ->join('l.teams', 't')
            ->where('l.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getArrayResult();

        return (!empty($league)) ? $league : null;
    }

    public function deleteLeagueById($id)
    {
        $league = $this->find($id);
        if(!empty($league))
        {
            $this->_em->remove($league);
            $this->_em->flush();
            $this->_em->clear();
            return true;
        }
        else
        {
            return false;
        }
        
    }
}
