<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use \Firebase\JWT\JWT;

class AuthController extends Controller
{

    /**
     * @Route("/auth/get-token", name="get-token", methods={"POST"})
     * 
     * @param Request
     *
     * @return JsonResponse
     *
     * This function generate JWT and return it with response json
     */
    public function getJWTKeyAction(Request $request)
    {

      try
        {
            $key = $this->container->getParameter('jwt_key');
            $header =   '{
                          "alg": "HS256",
                          "typ": "JWT"
            }';

            $payload =  '{
                          "username": "'.$request->request->get('username').'",
                          "timestamp": '.time().'
            }';

            $jwt = JWT::encode($payload, $key);

            if($jwt)
            {
                $json = ['status' => '1', 'message' => 'Success','token' => $jwt];
                $status = Response::HTTP_OK;
            }
            else
            {
                $json = ['status' => 'Failed','token' => null];
                $status = Response::HTTP_BAD_REQUEST;
            }

            return new JsonResponse($json, $status);
        }
        catch(\Exception $e)
        {
            $status = Response::HTTP_BAD_REQUEST;
            return new JsonResponse(["status" => "0", "message" => $e->getMessage()], $status);
        }

    }
}
