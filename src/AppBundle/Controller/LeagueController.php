<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Leagues;
use AppBundle\Entity\Teams;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use \Firebase\JWT\JWT;

class LeagueController extends Controller
{

    protected $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="homepage")
     *
     * This function doing nothing at this point
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        echo "Home Page"; die();
    }

    /**
     * @Route("/api/league/{id}", name="list-teams", methods={"GET"}, requirements={"id"="\d+"})
     *
     * @param Request, SerializerInterface, id
     *
     * @return Response
     *
     * This function return all the teams under any given league ass json
     */
    public function listTeamsAction(Request $request, SerializerInterface $serializer, $id)
    {

        try
        {
            $teams = $this->getDoctrine()->getRepository( Leagues::class )->findTeamsByLeagueId($id);

            if(!empty($teams))
            {
                $json = $this->serializer->serialize(["data" => $teams, "status" => "1", "message" => "Success"], 'json');
                $status = Response::HTTP_OK;
            }
            else
            {
                $json = ["data" => "", "status" => "0", "message" => "Failed"];
                $status = Response::HTTP_BAD_REQUEST;
            }

            return new Response($json, $status, ['Content-type' => 'application/json']);

        }catch(\Exception $e)
        {
            $json = $this->serializer->serialize(["status" => "0", "message" => $e->getMessage()], 'json');
            $status = Response::HTTP_BAD_REQUEST;
            return new Response($json, $status, ['Content-type' => 'application/json']);
        }
        
    } 

    /**
     * @Route("/api/league/{id}", name="delete-team", methods={"DELETE"})
     *
     * @param Request, id
     *
     * @return JsonResponse
     *
     * This function delete league which in turn unlink all the teams of that league too
     */
    public function deleteLeagueAction(Request $request, $id)
    {

        try
        {

            $deleteFlag = $this->getDoctrine()->getRepository( Leagues::class )->deleteLeagueById($id);

            if($deleteFlag)
            {
                $json = $this->serializer->serialize(["status" => "1", "message" => "Success"], 'json');
                $status = Response::HTTP_OK;
            }
            else
            {
                $json = $this->serializer->serialize(["status" => "0", "message" => "Faileddddd"], 'json');
                $status = Response::HTTP_BAD_REQUEST;
            }

            return new Response($json, $status, ['Content-type' => 'application/json']);

        }
        catch(\Exception $e)
        {
            $json = $this->serializer->serialize(["status" => "0", "message" => $e->getMessage()], 'json');
            $status = Response::HTTP_BAD_REQUEST;
            return new Response($json, $status, ['Content-type' => 'application/json']);
        }

        
    }
}
