<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Leagues;
use AppBundle\Entity\Teams;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use \Firebase\JWT\JWT;

class TeamController extends Controller
{

    protected $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/league/team", name="add-team", methods={"POST"})
     *
     * @param Request
     *
     * @return Response
     *
     * This function add one single team with given values
     */
    public function addTeamAction(Request $request)
    {
        try
        {

            $lastInsertedId = $this->getDoctrine()->getRepository( Teams::class )->addTeam($request->request->all());

            if(!empty($lastInsertedId))
            {
                $json = $this->serializer->serialize(["status" => "1", "message" => "Success"], 'json');
                $status = Response::HTTP_OK;
            }
            else
            {
                $json = $this->serializer->serialize(["status" => "0", "message" => "Failed"], 'json');
                $status = Response::HTTP_BAD_REQUEST;
            }

            return new Response($json, $status, ['Content-type' => 'application/json']);
        }
        catch(\Exception $e)
        {
            $json = $this->serializer->serialize(["status" => "0", "message" => $e->getMessage()], 'json');
            $status = Response::HTTP_BAD_REQUEST;
            return new Response($json, $status, ['Content-type' => 'application/json']);
        }
    }

    /**
     * @Route("/api/league/team", name="update-team", methods={"PUT"})
     * 
     * @param Request
     *
     * @return Response
     *
     * This function update team with given values
     */
    public function updateTeamAction(Request $request)
    {

        try
        {

            $data = json_decode($request->getContent(), true);

            $lastUpdatedId = $this->getDoctrine()->getRepository( Teams::class )->updateTeam($data);

            if(!empty($lastUpdatedId))
            {
                $json = $this->serializer->serialize(["status" => "1", "message" => "Success"], 'json');
                $status = Response::HTTP_OK;
            }
            else
            {
                $json = $this->serializer->serialize(["status" => "0", "message" => "Failed"], 'json');
                $status = Response::HTTP_BAD_REQUEST;
            }
            return new Response($json, $status, ['content-type' => 'application/json']);

        }
        catch(\Exception $e)
        {
            $json = $this->serializer->serialize(["status" => "0", "message" => $e->getMessage()], 'json');
            $status = Response::HTTP_BAD_REQUEST;
            return new Response($json, $status, ['Content-type' => 'application/json']);
        }

    }

}
