-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `leagues`;
CREATE TABLE `leagues` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `leagues` (`id`, `name`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Premier League', NULL, NULL, 1),
(2, 'FA cup', NULL, NULL, 1),
(3, 'FA Community Shield',  NULL, NULL, 1);

DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `league_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `strip` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `league_id` (`league_id`),
  CONSTRAINT `teams_ibfk_2` FOREIGN KEY (`league_id`) REFERENCES `leagues` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `teams` (`id`, `league_id`, `name`, `strip`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 1,  'Arsenal',  NULL, '2018-08-31 18:06:11',  '2018-08-31 17:45:45',  1),
(2, 1,  'Barnsley', NULL, '2018-08-31 18:06:25',  '2018-08-31 18:02:53',  1),
(3, 1,  'Crystal Palace', '', '2018-08-31 17:42:51',  NULL, 1),
(4, 1,  'Fulham', '', '2018-08-31 17:42:51',  NULL, 1),
(5, 1,  'Huddersfield Town',  '', '2018-08-31 17:42:51',  NULL, 1),
(6, 1,  'Burnley',  '', '2018-08-31 17:42:51',  NULL, 1),
(7, 1,  'Southampton',  '', '2018-08-31 17:42:51',  NULL, 1),
(8, 1,  'Brighton & Hove Albion', '', '2018-08-31 17:42:51',  NULL, 1),
(9, 2,  'Burnley',  '', NULL, NULL, 1),
(10,  2,  'Cardiff City', '', NULL, NULL, 1),
(11,  2,  'Chelsea',  '', NULL, NULL, 1),
(12,  2,  'Everton',  '', NULL, NULL, 1),
(13,  2,  'Leicester City', '', NULL, NULL, 1),
(14,  2,  'Liverpool',  '', NULL, NULL, 1),
(15,  2,  'Manchester City',  '', NULL, NULL, 1),
(16,  2,  'Manchester United',  '', NULL, NULL, 1),
(17,  3,  'Newcastle United', '', '2018-08-31 18:04:48',  NULL, 1),
(18,  3,  'Tottenham Hotspur',  '', '2018-08-31 18:04:48',  NULL, 1),
(19,  3,  'Watford',  '', '2018-08-31 18:04:48',  NULL, 1),
(20,  3,  'West Ham United',  '', '2018-08-31 18:04:48',  NULL, 1),
(21,  3,  'Wolverhampton Wanderers',  '', '2018-08-31 18:04:48',  NULL, 1),
(22,  3,  'Old Etonians', '', '2018-08-31 18:04:48',  NULL, 1),
(23,  3,  'Nottingham Forest',  '', '2018-08-31 18:04:48',  NULL, 1),
(24,  3,  'Sunderland', '', '2018-08-31 18:04:48',  NULL, 1);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'football', '$2y$10$wJL0DCbhGk.Ikj3ctxvqGeBQVswZSmggoPCJbed/foi4KyW/qhITe', 'ROLE_USER');

-- 2018-08-31 18:08:34